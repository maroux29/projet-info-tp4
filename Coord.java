/*----------------------------------------------------------*
 * Coord : Class qui permet d'initialiser les durées des Etats*
 *                                                          *
 * Attribut : x et y , indiquant les coordonnées de notre   *
 *            individu.                                     *
 *----------------------------------------------------------*/

public class Coord {
    private int x;
    private int y;

    public Coord(int x, int y)// initialisation en rentrant des coordonnées.
    {
        this.x = x;
        this.y = y;
    }

    public int getCoordX() {
        return x;
    }

    public int getCoordY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof Coord) {
            Coord other = (Coord) obj;
            return this.x == other.x && this.y == other.y;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 31 * x + y;
    }
}