import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*--------------------------------------------- *
* Classe Grille : permet de créer notre grille  *
*                                               *
* Attribut : tab2d , un tableau en deux         * 
*            dimension avec des liste d'individu*
*			 a chaque case.                     *
*-----------------------------------------------*/

@SuppressWarnings("unchecked")
public class Grille {

	private List<Individu>[][] tab2d;

	public Grille() {
		tab2d = new List[300][300];
		for (int i = 0; i < 300; i++) {
			for (int j = 0; j < 300; j++) {
				tab2d[i][j] = new ArrayList<>();
			}
		}
	}

	/* getter */
	public List<Individu>[][] getTab2d() {
		return tab2d;
	}
}
