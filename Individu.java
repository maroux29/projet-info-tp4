/*----------------------------------------------------------*
 * Individu : Class représentant un individu.               *
 *                                                          *
 * Attribut : statut , une lettre représentant son statut   *
 *            (S,E,I,R)                                     *
 *                                                          *
 *            tempsEcoule , entier représentant le temps    *
 *            écoulé dans un état                           *
 *                                                          *
 *            duree , représentant la durée des Etats       *
 *                                                          *
 *            coord , représentant les coordonnées de notre *
 *            individu                                      *
 *                                                          *
 *            newCoord , représentant les coordonées de     *
 *            notre individu au prochain jours              *
 *                                                          *   
 *            id , un entier représentant un identifiant    *
 *----------------------------------------------------------*/

public class Individu {

    private char statut;
    private int tempsEcoule;// dans un etat
    private Etat duree;
    private int nbVoisinsMalade;
    private Coord coord;
    private Coord newCoord;
    private int id;
    private static MTRandom random = new MTRandom(1234567);;// initialisation de notre mersenne twister

    public Individu(char statut, Coord coord, Coord newCoord, int id) {
        this.statut = statut;
        this.tempsEcoule = 0;
        this.coord = coord;
        this.newCoord = newCoord;
        this.duree = new Etat();
        this.nbVoisinsMalade = 0;
        this.id = id;

    }
    /*-----------------------------------------------------------*
     * changerEtat : Méthode qui permet de changer d'état selon  *
     *               le temps écoulé dans un état. Si il n'y a   *
     *               pas de changement d'état , on incrémente le *
     *               temps passé dans un état                    *
     *                                                           *
     * Entrée : rien                                             *
     *                                                           *
     * sortie : rien , modification potentielle du statut        *
     *-----------------------------------------------------------*/

    public void changerEtat() {
        switch (this.statut) {
            case 'E': // si notre individu est de statue 'E'
                if (this.tempsEcoule > this.duree.getDE()) {
                    this.statut = 'I';
                    this.tempsEcoule = 0;
                } else {
                    this.tempsEcoule++;
                }

                break;
            case 'I':
                if (this.tempsEcoule > this.duree.getDI()) {
                    this.statut = 'R';
                    this.tempsEcoule = 0;
                } else {
                    this.tempsEcoule++;
                }
                break;
            case 'R':
                if (this.tempsEcoule > this.duree.getDR()) {
                    this.statut = 'S';
                    this.tempsEcoule = 0;
                } else {
                    this.tempsEcoule++;
                }
                break;
            case 'S':
                double proba = 1 - Math.exp(-0.5 * this.nbVoisinsMalade);
                if (this.statut == 'S' && this.random.nextFloat() < proba) {
                    this.statut = 'E';
                    this.tempsEcoule = 0;
                }
                this.nbVoisinsMalade = 0;
                break;
        }
    }

    /* Voici tout nos getter */

    public Etat getDuree() {
        return this.duree;
    }

    public char getStatut() {
        return this.statut;
    }

    public int getTemps() {
        return this.tempsEcoule;
    }

    public int getId() {
        return this.id;
    }

    public Coord GetCoord() {
        return this.coord;
    }

    public Coord GetNewCoord() {
        return this.newCoord;
    }

    /* Voici tout nos setter */

    public void setCoord(Coord new_coord) {
        this.coord = new_coord;
    }

    public void setNewCoord(Coord new_coord) {
        this.newCoord = new_coord;
    }

    public void setNbVoisinsMalade(int n) {
        this.nbVoisinsMalade = n;
    }

    /*
     * Methode incrementNBVoisinsMalade : permet d'incrémenter le nombre de voisin
     * malade
     */
    public void incrementNbVoisinsMalade() {
        this.nbVoisinsMalade++;
    }

    public static void main(String[] args) {
        random = new MTRandom(123456789);// initialisation de notre mersenne twister
    }

}
