import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* class Main : Notre programme principal */
/* pas d'attribut                         */

public class Main {

	public static Grille grille;

	/*
	 * methode nouv_individu : permet d'initaliser un individu *
	 * et de le placer dans notre grille *
	 * *
	 * Entrée : notre grille , un lettre représentant le statut *
	 * deux entier x et y représentant les coordonées *
	 * un entier id représentant l'id *
	 * *
	 * Sortie : rien *
	 * ----------------------------------------------------------
	 */

	public static void nouv_individu(Grille grille, char statut, int x, int y, int id) {

		Individu individu = new Individu(statut, new Coord(x, y), new Coord(x, y), id);

		int a = individu.GetCoord().getCoordX();
		int b = individu.GetCoord().getCoordY();
		grille.getTab2d()[a][b].add(individu);
	}

	/*
	 * methode checkAlentour : permet de vérifier si ya des *
	 * individu infecté dans les cases *
	 * autour et d'incrémenter le *
	 * nbVoisinsMalades *
	 * *
	 * Entrée : notre grille *
	 * *
	 * un individu *
	 * *
	 * deux entier i et j représentant les coordonnées *
	 * de notre individu *
	 * *
	 * Sortie : rien *
	 * ----------------------------------------------------------
	 */

	public static void checkAlentour(Grille grille, Individu individu, int i, int j) {
		int i_moins;
		int j_moins;
		if (i == 0) {
			i_moins = 299;
		} else {
			i_moins = i - 1;
		}
		if (j == 0) {
			j_moins = 299;
		} else {
			j_moins = j - 1;
		}
		int j_plus = (j + 1) % 300;
		int i_plus = (i + 1) % 300;
		List<Individu> listeActuelle = grille.getTab2d()[i][j];
		if (!listeActuelle.isEmpty()) // si liste pas vide alors
		{
			for (Individu individu2 : listeActuelle)// parcourt de la liste dans cette cordonnée
			{
				if (individu2.getStatut() == 'I') {
					individu.incrementNbVoisinsMalade();
				}
			}
		}
		listeActuelle = grille.getTab2d()[i_moins][j];// individu en haut
		if (!listeActuelle.isEmpty()) // si liste pas vide alors
		{
			for (Individu individu2 : listeActuelle)// parcourt de la liste dans cette cordonnée
			{
				if (individu2.getStatut() == 'I') {
					individu.incrementNbVoisinsMalade();
				}
			}
		}
		listeActuelle = grille.getTab2d()[i_moins][j_moins];// individu en haut a gauche
		if (!listeActuelle.isEmpty()) // si liste pas vide alors
		{
			for (Individu individu2 : listeActuelle)// parcourt de la liste dans cette cordonnée
			{
				if (individu2.getStatut() == 'I') {
					individu.incrementNbVoisinsMalade();
				}
			}
		}
		listeActuelle = grille.getTab2d()[i][j_moins];// individu Ã gauche
		if (!listeActuelle.isEmpty()) // si liste pas vide alors
		{
			for (Individu individu2 : listeActuelle)// parcourt de la liste dans cette cordonnée
			{
				if (individu2.getStatut() == 'I') {
					individu.incrementNbVoisinsMalade();
				}
			}
		}
		listeActuelle = grille.getTab2d()[i_plus][j];// individu en bas
		if (!listeActuelle.isEmpty()) // si liste pas vide alors
		{
			for (Individu individu2 : listeActuelle)// parcourt de la liste dans cette cordonnée
			{
				if (individu2.getStatut() == 'I') {
					individu.incrementNbVoisinsMalade();
				}
			}
		}
		listeActuelle = grille.getTab2d()[i_plus][j_plus];// individu en bas a droite
		if (!listeActuelle.isEmpty()) // si liste pas vide alors
		{
			for (Individu individu2 : listeActuelle)// parcourt de la liste dans cette cordonnée
			{
				if (individu2.getStatut() == 'I') {
					individu.incrementNbVoisinsMalade();
				}
			}
		}
		listeActuelle = grille.getTab2d()[i][j_plus];// individu Ã droite
		if (!listeActuelle.isEmpty()) // si liste pas vide alors
		{
			for (Individu individu2 : listeActuelle)// parcourt de la liste dans cette cordonnée
			{
				if (individu2.getStatut() == 'I') {
					individu.incrementNbVoisinsMalade();
				}
			}
		}
	}

	/*
	 * methode supprimerIndividu : permet de supprimer un *
	 * individu infecté dans la *
	 * grille *
	 * *
	 * Entrée : notre grille *
	 * *
	 * un individu *
	 * *
	 * Sortie : rien *
	 * ----------------------------------------------------------
	 */

	public static void supprimerIndividu(Grille grille, Individu individu) {
		int i = individu.GetCoord().getCoordX();
		int j = individu.GetCoord().getCoordY();
		List<Individu> listeActuelle = grille.getTab2d()[i][j];
		listeActuelle.remove(individu);
	}

	/*
	 * methode ajouterIndividu : permet d'ajouter un individu *
	 * dans notre grille *
	 * *
	 * Entrée : notre grille *
	 * *
	 * un individu *
	 * *
	 * Sortie : rien *
	 * ----------------------------------------------------------
	 */

	public static void ajouterIndividu(Grille grille, Individu individu) {
		int i = individu.GetNewCoord().getCoordX();
		int j = individu.GetNewCoord().getCoordY();
		List<Individu> listeActuelle = grille.getTab2d()[i][j];
		listeActuelle.add(individu);
	}

	public static void main(String[] args) {

		MTRandom random = new MTRandom(1234567); // initialisation de mersenne twister
		grille = new Grille(); // initialissation de notre grille
		int nb_I = 0, nb_S = 0, nb_E = 0, nb_R = 0; // initialisation de nos compteur d'etat
		int x, y;
		int taille_liste = 0;
		for (int nb_fichier = 0; nb_fichier < 100; nb_fichier++) {
			try {
				FileWriter writer = new FileWriter("resultats" + Integer.toString(nb_fichier + 1) + ".csv");// creation
																											// de notre
																											// fichier
																											// csv
				writer.append("Jour;Nb_E;Nb_I;Nb_R;Nb_S\n");
				for (int i = 0; i < 19980; i++) {
					nouv_individu(grille, 'S', Math.abs(random.nextInt() % 300), Math.abs(random.nextInt() % 300), i); // remplacer
																														// les
																														// 0
																														// par
																														// un
																														// nombre
																														// aleatoire

				}
				for (int i = 0; i < 20; i++) {
					nouv_individu(grille, 'I', Math.abs(random.nextInt() % 300), Math.abs(random.nextInt() % 300),
							19980 + i);// remplacer les 0 par un nombre aleatoire
				}

				ArrayList<Individu> stock = new ArrayList<>(); // permet de faire un stock des individu ayant été
																// parcouru

				for (int jour = 0; jour < 730; jour++) {
					nb_E = 0;
					nb_I = 0;
					nb_R = 0;
					nb_S = 0;
					for (int i = 0; i < 300; i++)// parcourt de la hashmap
					{
						for (int j = 0; j < 300; j++) {
							List<Individu> listeActuelle = grille.getTab2d()[i][j];// parcourt de la liste dans une case
																					// du tableau
							if (!listeActuelle.isEmpty()) // si liste pas vide alors
							{
								taille_liste = taille_liste + listeActuelle.size();
								for (Individu individu : listeActuelle)// parcourt de la liste dans cette cordonnée
								{
									switch (individu.getStatut())// incrémentation du nombre d'individu dans cet etat
									{
										case 'E':
											nb_E++;
											break;
										case 'I':
											nb_I++;
											break;
										case 'R':
											nb_R++;
											break;
										case 'S':
											nb_S++;
											break;
									}
									if (individu.getStatut() == 'S')// si l'individu.status == "S" alors checkez dans
																	// les cordonnée x-1 , y-1 ,x+1,y+1,x+1 et y+1,x+1
																	// et y-1, x-1 et y-1,x-1 et y+1 et incrémenter le
																	// nombre de machin.
									{
										checkAlentour(grille, individu, i, j);
									}
									individu.changerEtat();
									individu.setNewCoord(new Coord(Math.abs(random.nextInt() % 300),
											Math.abs(random.nextInt() % 300)));// nouvelle coordonée d'un individu
									individu.setNbVoisinsMalade(0); // on remet le nombre de voisin malade Ã 0
									stock.add(individu); // on ajoute l'individu dans notre stock
								} // parcourt de la liste dans la case
							} // if

						} // boucle j
					} // boucle i

					for (int i = 0; i < 300; i++) {
						for (int j = 0; j < 300; j++) {
							grille.getTab2d()[i][j].clear();// on vide les liste notre tableau
						}
					}
					if (jour < 729) {// si on est pas au dernier jour
						for (Individu individu : stock) {
							x = individu.GetCoord().getCoordX();
							y = individu.GetCoord().getCoordY();
							individu.setCoord(new Coord(x, y));
							ajouterIndividu(grille, individu); // on remplit le tableau avec les individu dans leur
																// nouveau coordonnée

						}
					}
					stock.clear();// on vide notre stock

					writer.append(String.valueOf(jour)).append(";")// on remplit notre fichier csv
							.append(String.valueOf(nb_E)).append(";")
							.append(String.valueOf(nb_I)).append(";")
							.append(String.valueOf(nb_R)).append(";")
							.append(String.valueOf(nb_S)).append("\n");

				} // boucle jour
					// System.out.println(nb_fichier);
					// System.out.println(nb_E + " " + nb_I +" "+ nb_R + " "+ nb_S);
				writer.close();// on ferme le fichier csv
				System.out.println("succes");
			} catch (IOException e) {
				System.out.println("Error");
				e.printStackTrace();
			}
		}
	}
}
