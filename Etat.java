/*----------------------------------------------------------*
 * Etat : Class qui permet d'initialiser les durées des Etats*
 *                                                          *
 * Attribut : dE,dI,dR , des réels indiquant le temps       *
 *            nécessaire (en jour) pour changer d'états     *
 *----------------------------------------------------------*/

public class Etat {

    private double dE, dI, dR;
    private static MTRandom random = new MTRandom(1234567);
    /*------------------------------------------------------*
     *Accesseur : Etat() ne prend pas d'argunment           *
     *Permet d'initialiser la durée des Etats               *
     *------------------------------------------------------*/

    public Etat() {
        this.dE = negExp(3.0);
        this.dI = negExp(7.0);
        this.dR = negExp(365.0);
    }

    /*------------------------------------------------------*
     * negExp : Méthode qui retourne un nombre utilisant la *
     *          loi exponentielle négative , qui nous       *
     *          permettra de calculer la durée de chaque    *
     *          état selon la valeurs                       *
     * entree :                                             *
     *      -inMean : varie soit 3.0 pour dE , 7.0 pour dI, *
     *                365.0 pour dR                         *
     * sortie: un flottant correspondant Ã  la duree de      *
     *         l'état                                       *
     *------------------------------------------------------*/
    public double negExp(double inMean)// pout la durée d'expo , d'infection et de recovery
    {
        return -inMean * Math.log(1 - random.nextFloat());// faut importer mersenne twister

    }

    /* Tout les getteur */
    public double getDE() {
        return dE;
    }

    public double getDI() {
        return dI;
    }

    public double getDR() {
        return dR;
    }

    public static void main(String[] args) {
        random = new MTRandom(123456789);
    }
}
